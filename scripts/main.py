from scripts.persistence_utils import compute_bottleneck, compute_levelset_persistence, read_persistence_diag, compute_adjv_persistence, find_avg_h1
from scripts.setup import city_setup, spider_setup, rgg_setup, lattice_setup, ws_setup, snowflake_setup
import scripts.levelset as ls
from PIL import Image
import os
import scripts.visualize_simplicial_complexes as vsc
from scripts.plot_utils import plot_clusters_on_map, plot_density, plot_diagram, plot_dendrogram
from scripts.clustering_utils import cluster_diags, find_cluster_percentages_by_p0
import matplotlib.pyplot as plt
import numpy as np
import scripts.config as config
import networkx as nx
from matplotlib.colors import ListedColormap


def get_ls_complex_from_image(base_dir, img_path, application, place_name):
    """

    :return: tuple (list of simplices, corresponding entry times)
    """
    img_array = plt.imread(img_path)
    if len(img_array.shape) == 3:
        img = Image.open(img_path)
        img = img.convert('L')
        img_array = np.array(img)
        if application == 'city/inter/' or application == 'city/intra/':
            img_array = 1 - img_array
    return ls.build_levelset_complex(img_array,
                                     place_name,
                                     save_2d_plots=True,
                                     save_3d_plots=True,
                                     save_2d_dir=f'{base_dir}figures/levelset/2d/{application}',
                                     save_3d_dir=f'{base_dir}figures/levelset/3d/{application}',
                                     skip_int=30,
                                     save_sc=True,
                                     write_key=True,
                                     key_dir=f'{base_dir}results/keys/{application}',
                                     sc_dir=f'{base_dir}data/sc/{application}')


def levelset_workflow(base_dir, application_type, **kwargs):
    """workflow for running cities

    :param kwargs:
    :return:
    """

    if application_type == config.ApplicationType.INTERCITY:
        img_dir = f'{base_dir}figures/raw/city/inter/'
        application = 'city/inter/'
    elif application_type == config.ApplicationType.INTRACITY:
        img_dir = f'{base_dir}figures/raw/city/intra/'
        application = 'city/intra/'
    elif application_type == config.ApplicationType.SPIDERS:
        img_dir = f'{base_dir}figures/raw/spiders/'
        application = 'spiders/'
    elif application_type == config.ApplicationType.SNOWFLAKES:
        img_dir = f'{base_dir}figures/raw/snowflakes/'
        application = 'snowflakes/'
    elif application_type == config.ApplicationType.FIGS:
        img_dir = f'{base_dir}figures/raw/paperfigs/'
        application = 'paperfigs/'
    else:
        raise ValueError('Invalid Application Type')

    filename_list = [filename[:-4] for filename in sorted(os.listdir(img_dir)) if filename[-4:] == '.png'] # remove extension

    diags = []
    for filename in filename_list:
        diag = None

        if kwargs.get('compute_levelset', False):
            ls_simplex_list, entry_times = get_ls_complex_from_image(base_dir, f'{img_dir}{filename}.png', application, filename)

        if kwargs.get('gen_sc_vis', False):
            papercmp = ListedColormap(np.load('../../papercmap.npy'))
            vsc.visualize_simplicial_complex(f'{base_dir}figures/vis/{application}levelset/',
                                             filename,
                                             f'{base_dir}data/sc/{application}{filename}.dat',
                                             f'{base_dir}results/keys/{application}{filename}',
                                             f'{base_dir}data/sc/{application}{filename}_entry_times.csv',
                                             img=True,
                                             img_path=f'{img_dir}{filename}.png',
                                             animate=True, cmap=papercmp)

        if kwargs.get('compute_persistence', False):
            diag, st = compute_levelset_persistence(ls_simplex_list, entry_times)
            st.write_persistence_diagram(f'{base_dir}results/diagram/{application}{filename}')

        if kwargs.get('plot_diagram', False):
            plot_diagram(base_dir, application, filename, diag)

        if kwargs.get('plot_density', False):
            plot_density(base_dir, application, filename, diag)

        if kwargs.get('compute_bottleneck', False):
            if not diag:
                diag = read_persistence_diag(f'{base_dir}results/diagram/{application}{filename}')
            diags.append(diag)

    if kwargs.get('compute_bottleneck', False):
        np.save(f'{base_dir}results/dist_mat/{application}/distance.npy', compute_bottleneck(diags))

    if kwargs.get('compute_clusters', False):
        clusters, linkage_mat = cluster_diags(base_dir, application, kwargs.get('clustering_type', config.ClusteringType.HEIR),
                                 kwargs.get('num_clusters', 3))
        if kwargs.get('plot_clusters_on_map', False):
            plot_clusters_on_map(base_dir,
                                 application,
                                 kwargs.get('clustering_type', config.ClusteringType.HEIR),
                                 clusters,
                                 filename_list)
        if kwargs.get('plot_dendrogram', False):
            if application_type == config.ApplicationType.SPIDERS:
                plot_dendrogram(base_dir, application, kwargs.get('num_clusters', 3), linkage_mat, filename_list)
            elif application_type == config.ApplicationType.SNOWFLAKES:
                plot_dendrogram(base_dir, application, kwargs.get('num_clusters', 3), linkage_mat, filename_list)
            else:
                plot_dendrogram(base_dir, application, kwargs.get('num_clusters', 3), linkage_mat)


def adjv_workflow(base_dir, application_type, **kwargs):
    if application_type == config.ApplicationType.RGG:
        application = 'rgg/'
    elif application_type == config.ApplicationType.LATTICE:
        application = 'lattice/'
    elif application_type == config.ApplicationType.WS:
        application = 'ws/'
    raw_dir = f'{base_dir}data/raw/{application}'
    filename_list = [filename[:-4] for filename in sorted(os.listdir(raw_dir)) if filename[-4:] == '.gml'] # remove extension

    diags = []
    for filename in filename_list:
        diag = None
        if kwargs.get('use_computed_diag', False) and os.path.exists(f'{base_dir}results/diagram/{application}{filename}'):
            diag = read_persistence_diag(f'{base_dir}results/diagram/{application}{filename}')
            diags.append(diag)

        if kwargs.get('compute_persistence', False):
            G = nx.read_gml(f'{base_dir}data/raw/{application}{filename}.gml')
            diag, st = compute_adjv_persistence(G, 'inftime')
            st.write_persistence_diagram(f'{base_dir}results/diagram/{application}{filename}')

        if kwargs.get('plot_diagram', False):
            plot_diagram(base_dir, application, filename, diag, 10)

        if kwargs.get('plot_density', False):
            plot_density(base_dir, application, filename, diag, 10)

        if kwargs.get('compute_bottleneck', False):
            if not diag:
                diag = read_persistence_diag(f'{base_dir}results/diagram/{application}{filename}')
            diags.append(diag)

    if kwargs.get('compute_bottleneck', False):
        np.save(f'{base_dir}results/dist_mat/{application}/distance.npy', compute_bottleneck(diags))

    if kwargs.get('compute_clusters', False):
        clusters, linkage_mat = cluster_diags(base_dir, application, kwargs.get('clustering_type', config.ClusteringType.HEIR),
                                 kwargs.get('num_clusters', 3))
        if kwargs.get('plot_dendrogram', False):
            if application_type == config.ApplicationType.SPIDERS:
                plot_dendrogram(base_dir, application, kwargs.get('num_clusters', 3), linkage_mat, filename_list)
            else:
                plot_dendrogram(base_dir, application, kwargs.get('num_clusters', 3), linkage_mat)
        if application_type == config.ApplicationType.RGG:
            print(find_cluster_percentages_by_p0(clusters, filename_list))
    print(find_avg_h1(diags, filename_list))


if __name__ == '__main__':
    if config.simplex_type == config.SimplexType.LS:
        if config.run_setup:
            if config.application_type in [config.ApplicationType.INTERCITY, config.ApplicationType.INTRACITY]:
                city_setup(config.base_dir, config.env_dir, config.application_type, config.folder_structure, **config.ls_workflow)
            elif config.application_type == config.ApplicationType.SPIDERS:
                spider_setup(config.base_dir, config.folder_structure)
            elif config.application_type == config.ApplicationType.SNOWFLAKES:
                snowflake_setup(config.base_dir, config.folder_structure)
        levelset_workflow(config.base_dir, config.application_type, **config.ls_workflow)
    elif config.simplex_type == config.SimplexType.ADJV:
        if config.run_setup:
            if config.application_type == config.ApplicationType.RGG:
                rgg_setup(config.base_dir, config.folder_structure, **config.rgg_setup)
            elif config.application_type == config.ApplicationType.LATTICE:
                lattice_setup(config.base_dir, config.folder_structure, **config.lattice_setup)
            elif config.application_type == config.ApplicationType.WS:
                ws_setup(config.base_dir, config.folder_structure, **config.ws_setup)
        adjv_workflow(config.base_dir, config.application_type, **config.adjv_workflow)
