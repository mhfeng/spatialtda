import osmnx as ox
import gudhi as gd
import networkx as nx
import numpy as np


def compute_adjv_persistence(G, function_key):
    st = gd.SimplexTree()
    H = nx.convert_node_labels_to_integers(G)
    for u in H.nodes():
        st.insert([u], filtration=int(H.nodes[u][function_key]))
    for (u,v) in H.edges():
        if u != v:
            if v<u:
                print('HELP')
            st.insert([u, v], filtration=max(int(H.nodes[u][function_key]), int(H.nodes[v][function_key])))
        u_nb = set([n for n in H.neighbors(u)])
        v_nb = set([n for n in H.neighbors(v)])
        nbs = list(u_nb.intersection(v_nb))
        for w in nbs:
            if u != w and v != w:
                st.insert([u, v, w])
    st.make_filtration_non_decreasing()
    st.set_dimension(2)
    diag = st.persistence()
    return diag, st


def get_city_persistence(coord, dist=1000):
    """get persistence diagram from lat, long coordinate
    """
    G = ox.graph_from_point(coord, distance=dist, network_type='all')
    st = gd.SimplexTree()
    H = nx.convert_node_labels_to_integers(G)
    for (u, v, w) in H.edges.data('length'):
        if u != v:
            st.insert([u, v], filtration=w)

    st.make_filtration_non_decreasing()
    diag = st.persistence()
    return diag


def compute_levelset_persistence(simplex_list, entry_times):
    st = gd.SimplexTree()
    for simplex in simplex_list:
        filtration_val = max([entry_times[vertex] for vertex in simplex])
        st.insert(simplex, filtration=filtration_val)
    st.make_filtration_non_decreasing()

    diag = st.persistence()
    return diag, st


def read_persistence_diag(diag_path):
    """read persistence diagram file to a list

    :param diag_path: path to persistence diagram file
    :return: diagram in list format
    """
    diag = []
    with open(diag_path) as file:
        for line in file:
            line = line.split('\n')[0]
            pair = [float(l) for l in line.split(' ')[1:3]]
            if float(line.split(' ')[0]) == 1:
                diag.append(pair)
    return diag


def compute_bottleneck(diags):
    dist_mat = np.zeros((len(diags), len(diags)))
    for i in range(len(diags)):
        for j in range(len(diags) - i - 1):
            dist_mat[i, i + j + 1] = gd.bottleneck_distance(diags[i], diags[i + j + 1])
            dist_mat[i + j + 1, i] = dist_mat[i, i + j + 1]
    return dist_mat

def find_avg_h1(diags, file_key):
    num_h1 = np.zeros(len(file_key))
    p0 = np.zeros(len(file_key))
    h1_dict = {}
    for i in range(len(diags)):
        filename = file_key[i]
        params = filename.split('-')
        params = {params[i]: params[i+1] for i in range(0, len(params)-1, 2)}
        p0[i] = float(params['p0'])
        num_h1[i] = len(diags[i])
    for p0_val in np.linspace(0.05, 0.3, 6):
        p0_val = float('%.3f'%p0_val)
        h1_dict[p0_val] = np.average(num_h1[p0 == p0_val])
    return h1_dict