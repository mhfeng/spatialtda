import matplotlib.pyplot as plt
import gudhi as gd
from scripts.clustering_utils import cluster_list_to_df
import geopandas as gpd
from shapely import wkt
import pandas as pd
import scripts.config as config
import numpy as np
from scipy.cluster.hierarchy import dendrogram


def plot_diagram(base_dir, application, filename, diag, max_fil = 25):
    if not diag:
        gd.plot_persistence_diagram(persistence_file=f'{base_dir}results/diagram/{application}/{filename}')
    else:
        gd.plot_persistence_diagram(diag)
    # plt.axis([0, max_fil, 0, max_fil])

    plt.savefig(f'{base_dir}figures/diagrams/{application}/{filename}.png')
    plt.close()


def plot_density(base_dir, application, filename, diag):
    if not diag:
        gd.plot_persistence_density(persistence_file=f'{base_dir}results/diagram/{application}/{filename}',
                                    dimension=1,
                                    legend=True,
                                    max_intervals=0)
    else:
        gd.plot_persistence_density(diag, dimension=1, legend=True)

    plt.savefig(f'{base_dir}figures/density/{application}/{filename}.png')
    plt.close()


def plot_clusters_on_map(base_dir, application, cluster_type, clusters, file_key):
    if application == 'city/inter/':
        bd_shp_file = f'{base_dir}data/raw/shp/shang_dis_merged/shang_dis_merged.shp'
        point_csv_file = f'{base_dir}data/sample/{application}shanghai/shanghai.csv'
    elif application == 'city/intra/':
        bd_shp_file = f'{base_dir}data/raw/shp/Countries_WGS84/Countries_WGS84.shp'
        point_csv_file = f'{base_dir}data/raw/worldcities.csv'

    bd_gdf = gpd.read_file(bd_shp_file)
    point_df = pd.read_csv(point_csv_file)

    cluster_df = cluster_list_to_df(clusters, file_key, application)

    if application == 'city/inter/':
        new_gdf = gpd.GeoDataFrame(pd.merge(point_df, cluster_df, how='left', left_on=['neighborhood','index'], right_on=['neighborhood', 'index']))
        new_gdf['geometry'] = new_gdf['geometry'].apply(wkt.loads)
    elif application == 'city/intra/':
        point_df = point_df[point_df['population'] >= 1500000]
        new_gdf = gpd.GeoDataFrame(pd.merge(point_df, cluster_df, how='left', left_on=['city_ascii'], right_on=['city_ascii']))
        new_gdf['geometry'] = gpd.points_from_xy(new_gdf['lng'], new_gdf['lat'])
    else:
        new_gdf = gpd.GeoDataFrame({})
    new_gdf.crs = bd_gdf.crs

    fig, ax = plt.subplots(1,1)
    ax = bd_gdf.plot(ax=ax, color='white', edgecolor='gray')
    new_gdf.plot(ax=ax, column='cluster_number', markersize=4, cmap='Set1')

    num_clusters = len(clusters)
    if cluster_type == config.ClusteringType.MEDOID:
        cluster_str = 'med'
    elif cluster_type == config.ClusteringType.SPECTRAL:
        cluster_str = 'spec'
    elif cluster_type == config.ClusteringType.HEIR:
        cluster_str = 'heir'
    plt.savefig(f'{base_dir}/figures/clusters/{application}{cluster_str}-{num_clusters}-clusters.png')
    plt.close()


def plot_dendrogram(base_dir, application, num_clusters, linkage_mat, labels=None):
    dendrogram_file = f'{base_dir}figures/clusters/{application}heir-{num_clusters}-dendro.png'
    if labels:
        dendrogram(linkage_mat, p=num_clusters, truncate_mode='lastp', labels=labels, leaf_rotation=30)
    else:
        dendrogram(linkage_mat, p=num_clusters, truncate_mode='lastp',)
    plt.savefig(dendrogram_file)
    plt.close()


## Credit to James Boyle: https://scipy-cookbook.readthedocs.io/items/Matplotlib_Loading_a_colormap_dynamically.html
def gmtColormap(filePath):
    import colorsys
    import Numeric
    N = Numeric
    # if type(GMTPath) == type(None):
    #     filePath = "/usr/local/cmaps/" + fileName + ".cpt"
    # else:
    #     filePath = GMTPath + "/" + fileName + ".cpt"
    try:
        f = open(filePath)
    except:
        print
        "file ", filePath, "not found"
        return None

    lines = f.readlines()
    f.close()

    x = []
    r = []
    g = []
    b = []
    colorModel = "RGB"
    for l in lines:
        ls = l.split()
        if l[0] == "#":
            if ls[-1] == "HSV":
                colorModel = "HSV"
                continue
            else:
                continue
        if ls[0] == "B" or ls[0] == "F" or ls[0] == "N":
            pass
        else:
            x.append(float(ls[0]))
            r.append(float(ls[1]))
            g.append(float(ls[2]))
            b.append(float(ls[3]))
            xtemp = float(ls[4])
            rtemp = float(ls[5])
            gtemp = float(ls[6])
            btemp = float(ls[7])

    x.append(xtemp)
    r.append(rtemp)
    g.append(gtemp)
    b.append(btemp)

    nTable = len(r)
    x = N.array(x, N.Float)
    r = N.array(r, N.Float)
    g = N.array(g, N.Float)
    b = N.array(b, N.Float)
    if colorModel == "HSV":
        for i in range(r.shape[0]):
            rr, gg, bb = colorsys.hsv_to_rgb(r[i] / 360., g[i], b[i])
            r[i] = rr;
            g[i] = gg;
            b[i] = bb
    if colorModel == "HSV":
        for i in range(r.shape[0]):
            rr, gg, bb = colorsys.hsv_to_rgb(r[i] / 360., g[i], b[i])
            r[i] = rr;
            g[i] = gg;
            b[i] = bb
    if colorModel == "RGB":
        r = r / 255.
        g = g / 255.
        b = b / 255.
    xNorm = (x - x[0]) / (x[-1] - x[0])

    red = []
    blue = []
    green = []
    for i in range(len(x)):
        red.append([xNorm[i], r[i], r[i]])
        green.append([xNorm[i], g[i], g[i]])
        blue.append([xNorm[i], b[i], b[i]])
    colorDict = {"red": red, "green": green, "blue": blue}
    return (colorDict)