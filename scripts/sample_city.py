import sys
from shapely.geometry import Point
import geopandas as gpd
import random
import os
import pandas as pd


def random_points_in_polygon(N, polygon):
    """samples N points from a polygon geometry

    :param N: number of points to sample
    :param polygon: geometry to sample from
    :return: list of points
    """
    min_x, min_y, max_x, max_y = polygon.bounds

    points = gpd.GeoDataFrame()
    points['geometry'] = None
    points.set_geometry('geometry')
    points_list = []
    while len(points_list) < N:
        point = Point(random.uniform(min_x, max_x), random.uniform(min_y, max_y))
        if polygon.contains(point):
            points_list.append(point)
    points.set_geometry(points_list, inplace=True)
    return points


def sample_city(base_dir, city, N=100):
    """samples N points from each neighborhood of a city

    :param base_dir: base directory for project
    :param city: city to sample
    :param N: number of points to sample per neighbourhood
    :return: dictionary of points for each neighbourhood
    """
    shp_dir = f'{base_dir}data/raw/shp/'
    if city == 'shanghai':
        shp_dir = f'{shp_dir}shang_dis_merged/'
        shp_file = f'{shp_dir}shang_dis_merged.shp'
    else:
        # TODO: Fix this
        raise Exception

    geodata = gpd.read_file(shp_file)
    points_by_neighborhood = {}
    for i in range(len(geodata.geometry)):
        points = random_points_in_polygon(N, geodata.geometry[i])
        points.crs = geodata.crs
        points_by_neighborhood[geodata.Name[i]] = points

    return points_by_neighborhood


def intercity_dict(base_dir, city, N=100):
    """generates dictionary for intercity comparisoon

    :param base_dir: base directory of project
    :param city: city to run comparisons
    :return: dictionary of sampled points
    """
    intercity_dict = {}
    neighborhood_list = []
    index_list = []
    point_list = []

    points_by_neighborhood = sample_city(base_dir, city, N)
    if city == 'shanghai':
        intercity_dict = {
            'yugarden': (31.227372, 121.492480),
            'bund': (31.240518, 121.490609),
            'peoplessquare': (31.231457, 121.475423),
            'hongkou': (31.253286, 121.510959),
            'laoximen': (31.216894, 121.490021),
            'fc': (31.214661, 121.444830),
            'xujiahui': (31.193687, 121.438806),
            'centurypark': (31.214666, 121.549228),
            'pudong': (31.236619, 121.504651),
        }
        for area, coord in intercity_dict.items():
            neighborhood_list.append(area)
            index_list.append(0)
            point_list.append(Point(coord[1], coord[0]))
    for neighborhood, point_gdf in points_by_neighborhood.items():
        for i in range(len(point_gdf)):
            intercity_dict[f'{neighborhood}-{i}'] = (point_gdf.geometry[i].y, point_gdf.geometry[i].x)
            neighborhood_list.append(neighborhood)
            index_list.append(i)
            point_list.append(point_gdf.geometry[i])


    sample_data = gpd.GeoDataFrame(pd.DataFrame.from_dict({'neighborhood': neighborhood_list, 'index': index_list}),
                                   geometry = point_list)


    sample_dir = f'{base_dir}data/sample/city/inter/{city}/'
    if not os.path.exists(sample_dir):
        os.makedirs(sample_dir)
    sample_data.to_csv(f'{sample_dir}{city}.csv', encoding='utf-8')

    return intercity_dict


def intracity_dict(base_dir):
    """

    :return: intracity dictionary
    """
    cities_df = pd.read_csv(f'{base_dir}data/raw/worldcities.csv')
    capitals = cities_df[cities_df['population'] >= 1500000]
    intercity_dict = {}
    for i in range(capitals.shape[0]):
        intercity_dict[capitals.iloc[i,1]] = (capitals.iloc[i,2], capitals.iloc[i,3])
    return intercity_dict


if __name__ == '__main__':
    base_dir = sys.argv[1] if len(sys.argv) == 3 else '../'
    city = sys.argv[2] if len(sys.argv) == 3 else 'shanghai'
    print(generate_intercity_dictionary(base_dir, city))
